;****************************************
; Documentation
;****************************************

; Description:
; A drush makefile for the Pressflow core, version 7.

;****************************************
; General
;****************************************

; drush make API version
api = 2

; Drupal core
core = 7.x

;****************************************
; Pressflow core
;****************************************

projects[pressflow][type] = core
projects[pressflow][download][type] = git
projects[pressflow][download][url] = git://github.com/pressflow/7.git

; Patch from http://drupal.org/node/883038#comment-4554420
;projects[pressflow][patch][] = http://drupal.org/files/issues/ereg-suppress_warnings-883038-27.patch

; Patch from http://drupal.org/node/883038#comment-4897778
;projects[pressflow][patch][] = http://drupal.org/files/issues/ereg-suppress_warnings-883038-drushmake-clean-D6.patch  

;****************************************
; Patches
;****************************************

; Improvements to the core robots.txt file.
; https://redmine.koumbit.net/issues/2465
;projects[pressflow][patch][] = https://redmine.koumbit.net/projects/maj/repository/revisions/master/raw/patches/drupal-6.22/robots.txt.patch

; Empty $account->roles causes a sql error in user_access
; http://drupal.org/node/777116
;projects[pressflow][patch][] = http://drupal.org/files/issues/777116-no-roles-error-D6.patch

;****************************************
; End
;****************************************
